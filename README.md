# Bitly

### Objetivos Generales
<ol> 
<li>OB01</li> 
</ol>

### Objetivos Especificos
<ol>
<li>OB01</li>
<li>OB02</li>
<li>OB03</li>
<li>OB04</li>
</ol>

### Alcances
<ol>
<li>AL01</li>
<li>AL02</li>
<li>AL03</li>
<li>AL04</li>
<li>AL05</li>
</ol>

### Requerimientos Funcionales 
<ol>
<li>REQ01</li>
<li>REQ02</li>
<li>REQ03</li> 
<li>REQ04</li>

</ol>

### Requerimientos no Funcionales 
<ol> 
<li>REQ01</li>
<li>REQ02</li>
<li>REQ03</li>
</ol>
